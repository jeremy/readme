# Jeremy Watson's README

Hi, I’m Jeremy. I’m currently head of our contractor platform at [Remote](https://remote.com/) after serving as Head of Product. This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before.

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team. Please feel free to contribute to this page by opening a merge request. 

Thank you to the people who have contributed improvements or ideas to this document:
* Luca
* William
* Abhishek

## About me
I grew up in the Sacramento area of California (go Kings!) and recently lived in Philadelphia, PA in a little house in the Fishtown neighborhood. I've since moved to San Antonio, Texas! 
* I like taking photos and cooking things for other people. I shoot with a Sony A7R III and use my old Sony A6000 as a webcam on Zoom calls. It's a little overkill, but the creamy blurred background? Chef's kiss.
* I just got SCUBA certified and I'm excited to go diving in Hawaii this coming March. 
* I try my best to be actively helpful and kind.
* I like understanding how systems and people work. My parents described me as a quiet, intense kid that liked to take things around the house apart (particularly our VCR player) and put them back together.
  * I’m a great deal less serious as an adult, but I still like decomposing things to better understand them. This means I ask “why?” and “what’s the problem we’re trying to solve?” frequently.
* I try to listen first before acting and always listen deeply. I hope to be fully present in every conversation so I can develop deep empathy for people and their challenges.
* I have many strong opinions, but they’re weakly held. I love finding a new perspective that makes me change my opinion, because it means that I’ve gotten a chance to learn something substantive and new.
* I still struggle with imposter syndrome.

### Values that are extremely important to me
* Kindness: I strongly believe that you can be kind, empathetic, and caring to those around you while also building a great business. I strive to be a servant leader and make everyone else around me better; I feel enormously privileged and lucky to be doing what I do, and I think it's my responsibility to do as much as I can in service to others and give back out of gratitude.
* Collaboration: I have opinions that I'll defend, but the best solutions come when we work together as a team to find the best answer. I don't like working in a silo, and I don't wait until I have the "perfect answer" before I share an idea. You shouldn't, either!
* Diversity: Collaboration only works if we have a diverse set of voices at the table; otherwise, we're just building an echo chamber. It's important to me to do what I can to build a diverse, just, and equitable team and world: it's the right thing to do, but it also results in fundamentally better products. I find inequity abhorrent and I'll always do what I can to use my privilege and resources to level the playing field.

### 360 Feedback
In the spirit of transparency, I'll share a summary of 360 feedback from June 2020 in a previous role:
* My reports typically describe me as caring, focused on their professional development, and dedicated to making their roles easier. I've been described as willing to do whatever it takes to help the people around me whether it's writing documentation myself, taking notes in a meeting so someone else can focus, or brainstorming new ideas.
* I'm described as passionate about transparency, frequently asking people to move things to a public forum so other people can contribute.
* Someone described me as very action-oriented, never letting a discussion go without a clearly defined next step. "You put a lot of effort into living this value... I always walk away feeling satisfied with the answer or outcome."
* I'm described as "very empathetic" and someone who works hard to create a safe space for every employee to be their whole selves.
* Areas for improvement for me offered by others are working fewer hours ("... you tend to work long hours and are spread thin...") and more effective/tighter management of team results and direction.

#### Things I'm working on
* Efficiency: I'm still learning how to best use my time on high-leverage tasks. When I hear about a problem, I want to directly contribute to the solution - which isn't always the best use of my time. My TODO list tends to grow exponentially if I don't say "no" to things.
* Setting a good example: I tell my team to take lots of time off and protect their work-life balance; I monitor the amount of time the team takes, and bug people when they're not taking a minimum number of days off. However, I do a poor job of setting a good example by not working predictable hours and not watching over my own time off. I know that people look far closer at my actions instead of my words, so I'm working hard to improve here.
* Investing for the future: this is more tactical and specific to my role, but I'm trying to learn more about setting organizational level focus. As my lens zooms out of the feature level and tries to focus on the team as the product, I'm trying to continually learn what the best way is to set focus, get everyone excited about the direction, and make sure we're spending the right amount on the team to achieve our ambitious goals.

## My role
If you’re on my team, I’m here to support you, provide focus and clarity on our most important problems, and to advocate for you and the things you ship. I serve you, and we should optimize the things we do together for the organization.
First and foremost, I’m here to help you shine by enabling your success in your current role. I hope to do this in a few ways:
* Provide you with context. I do a lot of communicating with people you might not be otherwise exposed to, and I hope to shield you from things that aren’t as important and provide you with clarity on the things that are.
* Pair with you on problems. You’ll know your area of the product better than I do, but I’ll be close enough to have some informed thoughts of my own.
* Help you directly whenever you need it. I love hearing “I could really use help with...”. The best approach we can take together is preventing stress and anxiety from happening at all at work, but if I will always pick up a hose and firefight with you if needed and you provide that space for me.
I also want to be an avid supporter of your career path. Your career belongs to you, and I want to cheerlead and support you on your path - whether it’s continuing deeper into product management, people management, or not:
* Evangelize your success and watch you shine. I’ll do my best to celebrate your wins.
* Expose you to opportunities that allow you to grow and learn and encourage you to seize them. I don’t want you to be bored!

## How you can help me
* Do thoughtful, good work. Tell me if there's something preventing you from this, and we'll work together on fixing it.
* Default to action. Instead of waiting, move problems forward.
* Communicate. If there's a problem or you're blocked on something, please bring it up. I'll generally interpret silence as "everything's fine".
  * Don’t save urgent matters for a 1x1. Please bring them to my attention on Slack or by scheduling a separate call.
* Transparency is very important to me. Please default to using public channels.
* I like explicit asks. I’m better at helping when I have a good idea of what you need. “Take a look” is less helpful than “I’m looking for feedback on X and Y, by end of week”.
* Bring your whole self to work.

### What I assume about you
* Positive intent.
* You’re the DRI. You’re better at your job than I am! You know best, and you’ll tell me if there’s something preventing you from doing your best work possible.
* If I disagree with you, I may try to steer you down a different path. But me disagreeing with you doesn’t mean that you’re doing something wrong as long as you’re collaborating effectively with me and our teammates.
* You’ll ask for my input and help if it’s needed.
* Work is absolutely not the most important thing in your life. I assume that there are cherished relationships and personal interests that are more important to your happiness, and it’s hard to be happy with your job when work detracts from those things.

## My working style
* At times, I struggle to find a balance between my personal life and my work life. This is an area of growth for me, and I’m grateful to others when they chastise me for working too much.
* No weekend work. If you find yourself feeling pressure to work on the weekend, please bring it up with me immediately so we can solve for the root cause of that pressure together.
* Unless specifically mentioned, I don’t expect immediate responses from anyone. I respect others’ time and want you to have the space for a thoughtful response.
* I don’t keep Slack on my phone in an effort to stay disconnected from work during down time. Please consider doing the same.

### Communicating with me
* If we have a recurring 1x1, that time belongs to you. These interactions are very important to me and are dedicated to whatever topic you’re interested in discussing with me. They’re not status meetings unless you’d like them to be.
* I tend to make suggestions and asks, never commands. This can make my input unclear. If I’m not communicating expectations effectively, please say so and I’ll do my best to adjust my style. If I have a strong opinion about something, I will say so.
* I try to express gratitude frequently. This is genuine.
* I try to be a structured communicator and thinker, but sometimes start to speak in an unstructured way when I get excited about something. Please interject if you’re not getting what you want out of me.
* Sometimes, my calendar will be packed with meetings. Please DM me on Slack if you can’t find time, I will always make time for you.

